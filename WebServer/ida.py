from iottalkpy.dan import NoData
from flask_socketio import SocketIO, emit

### The registeration api url, you can use IP or Domain.
api_url = 'http://140.113.199.211:8080/csm'  # default
# api_url = 'http://localhost/csm'  # with URL prefix
# api_url = 'http://localhost:9992/csm'  # with URL prefix + port

### [OPTIONAL] If not given or None, server will auto-generate.
# evice_name = '99.Dummy_Test'

### [OPTIONAL] If not given or None, DAN will register using a random UUID.
### Or you can use following code to use MAC address for device_addr.
# from uuid import getnode
# device_addr = "{:012X}".format(getnode())
# device_addr = "..."

### [OPTIONAL] If the device_addr is set as a fixed value, user can enable
### this option and make the DA register/deregister without rebinding on GUI
# persistent_binding = True

### [OPTIONAL] If not given or None, this device will be used by anyone.
# username = 'myname'

### The Device Model in IoTtalk, please check IoTtalk document.
device_model = 'Test_Device'

### The input/output device features, please check IoTtalk document.
idf_list = ['Test_I']
odf_list = ['Test_O']

### Set the push interval, default = 1 (sec)
### Or you can set to 0, and control in your feature input function.
push_interval = 10  # global interval
interval = {
    'Test_I': 1,  # assign feature interval
}

def on_register():
    print('register successfully')


def Test_I(queue):
    if queue.empty():
        return NoData
    return queue.get()


def Test_O(data, queues, globals):  # data is a list
    print('Test_O received data: ' + str(data))
    queues['Test_I'].put(data)
    if data[0] == 'next_pic': 
        index = globals['index']
        index += 1
        if index > 10:
            index = 1
        globals['index'] = index
        socketio = SocketIO(message_queue='amqp://admin:admin@localhost:5672')
        socketio.emit('server_response', {'data': str(globals['index'])})
