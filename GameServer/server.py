from multiprocessing import Process, Queue, Manager
from flask import Flask, render_template, request
from flask_socketio import SocketIO, emit
import eventlet
eventlet.monkey_patch()

import dai

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app, async_mode='eventlet', message_queue='amqp://admin:admin@localhost:5672')

@app.route('/', methods=['GET', 'POST'])
def home():
    if request.method == 'POST':
        command = request.form.get('gallery', type=str)
        if sa.queues.get('Test_I'):
            sa.queues['Test_I'].put(command)
    return render_template('gamePage.html')

if __name__ == '__main__':
    sa = dai.module_to_sa(dai.load_module('ida.py'))
    sa.start()
    # Debug/Development
    # app.run(debug=True, host="0.0.0.0", port="5000")
    # Production
    socketio.run(app, host='0.0.0.0', port=5000)
    sa.terminate()